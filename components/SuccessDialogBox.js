var dom = require("@juspay/mystique-backend").doms.android;
var Connector = require("@juspay/mystique-backend").connector;
var View = require("@juspay/mystique-backend").baseViews.AndroidBaseView;
var LinearLayout = require("@juspay/mystique-backend").androidViews.LinearLayout;
var RelativeLayout = require("@juspay/mystique-backend").androidViews.RelativeLayout;
var TextView = require("@juspay/mystique-backend").androidViews.TextView;
var ImageView = require("@juspay/mystique-backend").androidViews.ImageView;
var FrameLayout = require("@juspay/mystique-backend").androidViews.FrameLayout;

import RadioButton from '../components/RadioButton';



class SuccessDialogBox extends View {
	constructor(props, children) {
		super(props, children);		
	}

	handleClick = () => {
		this.props.handleClick();
	}

	

	

	
render() {
		this.layout = (
			<RelativeLayout  
				width="match_parent" 
				height="match_parent"
				root="true">
			<LinearLayout 
				width="300"
				height="wrap_content"
				background="#ffffff"
				translationZ="30"
				gravity = "center"

				orientation="vertical"
				centerInParent="true,-1"
				padding="20,20,20,20">

					<LinearLayout
						width="match_parent"
						height="wrap_content"
						background="#FFFFFF"
						orientation="horizontal"
						weight="1">

							<LinearLayout
								width="match_parent"
								height="wrap_content"
								background="#FFFFFF"
								weight="0.15"
								gravity="center">
									<TextView 
										text={this.props.title} 
										fontSize="0,6"
										color="#d12a6e"/>
							</LinearLayout>

							

					</LinearLayout>


					<LinearLayout
						width="match_parent"
						height="wrap_content">

							<ImageView imageUrl="horizontal" />

					</LinearLayout>

					<LinearLayout
						width="match_parent"
						height="wrap_content"
						gravity="center"
						margin="0,10,0,10">

							<TextView 
								fontSize = "0,4" 
								text={this.props.message}
								gravity="center"/>

					</LinearLayout>

					<FrameLayout
						width="wrap_content"
						height="wrap_content"
						cornerRadius="15"
						>

						<ImageView
						imageUrl="axis_card_background"
						height="match_parent"
						width="match_parent"/>

						<LinearLayout
							width="match_parent"
							height="wrap_content"
							gravity = "center"
							orientation = "vertical"
							
							>



							<TextView 
									fontSize = "0,4" 
									text={this.props.vpa}
									gravity="center"
									color="#d12a6e"
									margin="0,40,0,0"/>

							<TextView 
									fontSize = "0,4" 
									text={this.props.account}
									gravity="center"
									margin="0,20,0,0"/>

							<TextView 
									fontSize = "0,4" 
									text={this.props.bank}
									gravity="center"
									margin="0,0,0,40"/>
								

						</LinearLayout>

					</FrameLayout>

					<LinearLayout
						width="match_parent"
						height="60"
						gravity="center"
						margin="0,20,0,0"
						background="#ffffff"
						weight="1">

							<LinearLayout
								width="match_parent"
								height="60"
								background="#ffffff"
								weight="0.5"
								gravity="center">

								<LinearLayout 
									width="100" 
									height="50" 
									background="#c1345a" 
									gravity="center" 
									cornerRadius="20"
									translationZ="10"
									onClick = {this.handleClick}>
										<TextView 
											text="PAY NOW"
											color="#FFFFFF"/>
								
								</LinearLayout>
							
							</LinearLayout>

							
							

					</LinearLayout>

			</LinearLayout>

			

			</RelativeLayout>);
		return this.layout.render();
	}

}

module.exports = SuccessDialogBox;