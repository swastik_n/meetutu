var dom = require("@juspay/mystique-backend").doms.android;
var Connector = require("@juspay/mystique-backend").connector;
var View = require("@juspay/mystique-backend").baseViews.AndroidBaseView;
var LinearLayout = require("@juspay/mystique-backend").androidViews.LinearLayout;
var RelativeLayout = require("@juspay/mystique-backend").androidViews.RelativeLayout;
var TextView = require("@juspay/mystique-backend").androidViews.TextView;
var ImageView = require("@juspay/mystique-backend").androidViews.ImageView;


import RadioButton from '../components/RadioButton';

const DEFAULTS = {
  TITLE : "",
  MESSAGE : "",
  POSITIVE_TEXT : "",
  NEGATIVE_TEXT : "",
  DUALVISIBLE : "gone",
  CLEARDIALOG : "visible",
  SINGLEBUTTON : "gone",
  DUALBUTTON : "visible"
}

class DialogBox extends View {
	constructor(props, children) {
		super(props, children);
		this.setDefaults();
		this.setIds([
			'visible',
			'rb1',
			'rb2'
		]);
		
	}

	setDefaults = () => {
    this.title = this.props.title || DEFAULTS.TITLE;
    this.message = this.props.message || DEFAULTS.MESSAGE;
    this.positivetext = this.props.positivetext || DEFAULTS.POSITIVE_TEXT;
    this.negativetext = this.props.negativetext || DEFAULTS.NEGATIVE_TEXT; 
    this.dualvisible = this.props.dualvisible || DEFAULTS.DUALVISIBLE;
    this.clearDialog = this.props.clearDialog || DEFAULTS.CLEARDIALOG;
    this.singleButton = this.props.singleButton || DEFAULTS.SINGLEBUTTON;
    this.dualButton = this.props.dualButton || DEFAULTS.DUALBUTTON;
	}


	negativeClick = ()=>  {
		if(this.props.negativeClick) {
			this.props.negativeClick();
		}
	}

	positiveClick = () => {
		if(this.props.positiveClick) {
			this.props.positiveClick();
		}
	}

	radioButtonSim1 = () => {

		var cmd = "";
		var _this =this;
		cmd += this.set({
  		id: _this.idSet.rb2,
  		checked : "false"
  		});
  		Android.runInUI(cmd, null);
  		this.props.checkedSimOne();

	}


	radioButtonSim2 = () => {
		var cmd = "";
		var _this =this;
		cmd += this.set({
  		id: _this.idSet.rb1,
  		checked : "false"
  		});
  		Android.runInUI(cmd, null);
  		this.props.checkedSimTwo();
	}

	render() {
		this.layout = (
			<RelativeLayout  
				width="match_parent" 
				height="match_parent"
				id = {this.idSet.visible}
				root="true">
			<LinearLayout 
				width="300"
				height="wrap_content"
				background="#ffffff"
				orientation="vertical"
				centerInParent="true,-1"
				padding="10,20,10,20">
					<LinearLayout
						width="match_parent"
						height="wrap_content"
						background="#FFFFFF"
						orientation="horizontal"
						weight="1">
							<LinearLayout
								width="match_parent"
								height="30"
								background="#FFFFFF"
								weight="0.2"
								gravity="center">
									<TextView 
										text={this.title}
										color="#FFCD1C5F" 
										fontSize="0,4"/>
							</LinearLayout>
							<LinearLayout
								width="match_parent"
								height="30"
								background="#FFFFFF"
								weight="0.8"
								gravity="center"
								onClick={this.negativeClick}
								visibility={this.clearDialog}>
									<ImageView 
										imageUrl="clear"
										width="24"
										height="24"
										margin="15,0,0,0"/>
							</LinearLayout>
					</LinearLayout>
					<LinearLayout
						width="match_parent"
						height="wrap_content">
							<ImageView imageUrl="horizontal" />
					</LinearLayout>
					<LinearLayout
						width="match_parent"
						height="wrap_content"
						minHeight="100">
							<TextView 
								fontSize = "0,4" 
								text={this.message}/>
					</LinearLayout>
					<LinearLayout
						width="match_parent"
						height="wrap_content"
						gravity = "center"
						margin="0,20,0,0"
						visibility = {this.dualvisible}
						orientation = "vertical">
							<RadioButton
								id = {this.idSet.rb1}
								width = "wrap_content"
								height ="wrap_content"
								text = "SIM 1"
								onClick = {this.radioButtonSim1}/>
							<RadioButton
								id = {this.idSet.rb2}
								width = "wrap_content"
								height ="wrap_content"
								text = "SIM 2"
								onClick = {this.radioButtonSim2}/>
					</LinearLayout>
					<LinearLayout
						width="match_parent"
						height="60"
						gravity="center"
						margin="0,20,0,0"
						background="#ffffff"
						weight="1"
						visibility={this.dualButton}>
							<LinearLayout
								width="match_parent"
								height="60"
								background="#ffffff"
								weight="0.5"
								gravity="center">
								<LinearLayout 
									width="100" 
									height="50" 
									background="#FFCD1C5F" 
									gravity="center" 
									cornerRadius="20"
									translationZ="10"
									onClick={this.positiveClick}>
										<TextView 
											text={this.positivetext} 
											color="#FFFFFF"/>
								</LinearLayout>
							</LinearLayout>
							<LinearLayout
								width="match_parent"
								height="60"
								background="#ffffff"
								weight="0.5"
								gravity="center">

								<LinearLayout 
									width="100" 
									height="50" 
									background="#FFCD1C5F" 
									gravity="center" 
									cornerRadius="20"
									translationZ="10"
									onClick={this.negativeClick}>
										<TextView
											 
											text={this.negativetext} 
											color="#FFFFFF"/>
								
								</LinearLayout>
							
							</LinearLayout>
							

					</LinearLayout>
					<LinearLayout
						width="match_parent"
						height="60"
						gravity="center"
						background="#ffffff"
						visibility={this.singleButton}>
						<LinearLayout
								width="match_parent"
								height="60"
								background="#ffffff"
								gravity="center">

								<LinearLayout 
									width="100" 
									height="50" 
									background="#FFCD1C5F" 
									gravity="center" 
									cornerRadius="20"
									translationZ="10"
									onClick={this.positiveClick}>
										<TextView 
											text={this.positivetext} 
											color="#FFFFFF"/>
								
								</LinearLayout>
							
							</LinearLayout>
					</LinearLayout>
			</LinearLayout>
			</RelativeLayout>);
		return this.layout.render();
	}

}

module.exports = DialogBox;