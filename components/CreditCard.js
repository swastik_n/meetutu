var dom = require("@juspay/mystique-backend").doms.android;
var Connector = require("@juspay/mystique-backend").connector;
var View = require("@juspay/mystique-backend").baseViews.AndroidBaseView;
var LinearLayout = require("@juspay/mystique-backend").androidViews.LinearLayout;
var RelativeLayout = require("@juspay/mystique-backend").androidViews.RelativeLayout;
var TextView = require("@juspay/mystique-backend").androidViews.TextView;
var ImageView = require("@juspay/mystique-backend").androidViews.ImageView;


import CardEditText from '../components/CardEditText';

import tinyColor from '../utils/tinyColor';
import Fab from '../components/Fab';






class CreditCard extends View {
	constructor(props, children) {
		super(props, children);

		this.props.appendText=this.props.appendText||"";

		this.setIds([
			'id'
		]);
	}






render() {


		this.layout = (
			
				<LinearLayout
					width="match_parent"
					height="wrap_content"
					background = "#ffffff"
					orientation="vertical"
					>
					
                    		
                    		
							
							<LinearLayout
								width="match_parent"
								height="wrap_content"
								orientation="horizontal"
								margin="30,30,30,30"
								>
								<RelativeLayout 
									width="match_parent" 
									height="wrap_content" 
									stroke="2,#c1345a"
									cornerRadius="15">
									<ImageView
										imageUrl="card"
										height="35"
										width="35"
										margin="10,5,0,5"/>
									<LinearLayout
										centerInParent= "true,-1"
										orientation = "horizontal">
										
										<CardEditText
											hint="Enter the last SIX digits of the Card" 
											gravity="center"
											fontSize="0,3"
											margin="0,0,5,0"
											initText= "Enter last 6 digits"
											appendText="XXXX-XXXX-XX-"
											errorText="Enter only the last six digits"
											entryType="Card"/>
										
										
									</LinearLayout>
								</RelativeLayout>
							</LinearLayout>
							<LinearLayout
								width="match_parent"
								height="wrap_content"
								orientation="horizontal"
								margin="30,30,30,30"
								>
								<RelativeLayout 
									width="match_parent" 
									height="wrap_content" 
									stroke="2,#c1345a"
									cornerRadius="15">
									<ImageView
										imageUrl="card_expirydate"
										height="35"
										width="35"
										margin="10,5,0,5"/>
									<LinearLayout
										centerInParent= "true,-1"
										orientation = "horizontal">
										
										<CardEditText
											hint="Enter Expiry Month/Year" 
											gravity="center"
											fontSize="0,3"
											margin="0,0,5,0"
											initText= "MM-YY"
											appendText=""
											errorText="Invalid Entry"
											entryType="Expiry"/>
										
										
									</LinearLayout>
								</RelativeLayout>
							</LinearLayout>

							
                    	
                </LinearLayout>
				
			
		)

		return this.layout.render();
	}
}

module.exports = CreditCard;


