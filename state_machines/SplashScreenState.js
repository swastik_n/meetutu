const objectAssign = require('object-assign');

var localState = {

}

module.exports = function(action, payload, state) {
  switch (action) {
    case "DEVICE_BINDING_RESPONSE":
      localState.deviceBindingResponse = payload
      localState.nextAction = "DEVICE_BINDING_RESPONSE_STORED"
      break;
    case "PERMISSION_CHECK":
      localState.permissionCheck = payload
      localState.nextAction = "PERMISSION_CHECK_DONE"
      break;
    case "PERMISSION_DENIED":
      localState.permissionCheck = payload
      localState.nextAction = "PERMISSION_DENIED"
      break;
    default:
      throw new Error("Invalid action Passed :  action name" + action);
  }

  return objectAssign({}, state, localState);
}