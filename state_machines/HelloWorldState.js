const objectAssign = require('object-assign');

var localState = {
	isInit: false,
	currScreen: null,
}

module.exports = function(action, payload, state) {
	var globalState = {
    currScreen : null
  }
	switch (action) {
		case "STORE_DATA":
			localState.message = payload.message
			localState.nextAction = "SAY_HELLO_WORLD";
			break;
		case "STORE_RESPONSE":
			localState.message = payload.message;
			localState.nextAction = "SHOW_NEXT_SCREEN";
			break;
		case "STORE_LOGIN_RESPONSE":
			localState.message = payload.message;
			localState.nextAction = "SHOW_LOGIN_SCREEN";
			break;
		default :
			throw new Error("Invalid action Passed :  action name" + action);

	}
	return objectAssign({}, state, {global : objectAssign({}, state.global, globalState), local: localState});
}