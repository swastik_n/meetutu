var path = require('path');
const WebpackShellPlugin = require('webpack-shell-plugin');
const webpack = require("webpack");
var plugins = [];

plugins.push(new webpack.optimize.UglifyJsPlugin({ output: {comments: false} }));

module.exports = {
  devtool: "source-map",
	entry: ["./config.js"],
  output: {
  	path: __dirname + "/dist", 
  	filename: "config_bundle.js", 
  	publicPath: '/dist/',
  	sourceMapFilename: "config_bundle.js.map"
  },
  plugins: plugins,
  module: {
    loaders: [
      {
        test: /\.js$/, 
        exclude: /node_modules\/(?!@juspay)/, 
        loader: "babel-loader"
      },
    ]
  }
}