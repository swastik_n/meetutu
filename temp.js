var dom = require("@juspay/mystique-backend").doms.android;
var Connector = require("@juspay/mystique-backend").connector;
var View = require("@juspay/mystique-backend").baseViews.AndroidBaseView;
var LinearLayout = require("@juspay/mystique-backend").androidViews.LinearLayout;
var RelativeLayout = require("@juspay/mystique-backend").androidViews.RelativeLayout;
var TextView = require("@juspay/mystique-backend").androidViews.TextView;
var ImageView = require("@juspay/mystique-backend").androidViews.ImageView;
var CheckBox = require("@juspay/mystique-backend").androidViews.CheckBox;
var ProgressBar = require("@juspay/mystique-backend").androidViews.ProgressBar;
const Olive = require("../services/Olive.js");
const callbackMapper = require("@juspay/mystique-backend/").helpers.android.callbackMapper;

import RadioButton from '../components/Axis-pay/RadioButton'

class LinkAccountScreen extends View {
  constructor(props, children, state) {
    super(props, children);
    this.vpa = state.createVpa.vpa;
    this.bank = state.createVpa.bank;
    this.setIds([
      "buttonblur",
      "image",
      "text",
      "actionbutton",
      "loading",
      "blur",
      "dot1",
      "dot2",
      "dot3"
    ]);
    var _this = this;
    setTimeout(() => {
      Android.runInUI(
        _this.animateView(0, 1, true),
        null
      )
    })
    setTimeout(() => {
      var toolbar = __ROOTSCREEN.find('toolbar')[0];
      var cmd = toolbar.show();
      cmd += toolbar.showBackButton("LINK BANK ACCOUNT ");
      Android.runInUI(cmd, null);
    }, 300);
    (__ROOTSCREEN).showLoading("Fetching Accounts");
    this.fetchAccounts();
  }

  showToast = (message) => {
    var cmd = "android.widget.Toast->makeText:ctx_ctx," + "cs_" + message + ",i_500;show;";
    Android.runInUI(cmd, null);
  };

  // In Error cases throw him back to select bank screen
  handleAccountResponse = (accountResponse) => {
    if (accountResponse) {
      if (accountResponse.error) {
        this.showToast("Error fetching bank accounts");
      } else {
        if (accountResponse.accounts) {
          if (accountResponse.accounts.length == 0) {
            this.showToast("No Accounts found");
          } else {
            this.accounts = accountResponse.accounts;
            this.account = this.accounts[0];
            (__ROOTSCREEN).showLoading("Generating Otp");
            this.setMpin(this.accounts[0]);
          }
        } else {
          this.showToast("Error fetching bank accounts");
        }
      }
    } else {
      this.showToast("Error fetching bank accounts");
    }
  };

  wasOtpGenerationSuccessful = (otpResponse) => {
    if (!otpResponse || otpResponse.error) {
      return false;
    }
    if (!otpResponse.otp || typeof otpResponse.otp != "object") {
      return false;
    }
    if (otpResponse.otp.generationStatus != "SUCCESS") {
      return false;
    }
    return true;
  };

  wasListKeysSuccessful = (listKeyResponse) => {
    if (!listKeyResponse || listKeyResponse.error) {
      return false;
    }
    if (!listKeyResponse.npciKeys || typeof listKeyResponse.npciKeys != "object") {
      return false;
    }
    if (listKeyResponse.npciKeys.result != "Success" || listKeyResponse.npciKeys.data == null) {
      return false;
    }
    return true;
  }

  wasNpciTokenFetchSuccessful = (npciTokenResponse) => {
    if (!npciTokenResponse || npciTokenResponse.error) {
      return false;
    }
    if (!npciTokenResponse.npciToken || typeof npciTokenResponse.npciToken != "object") {
      return false;
    }
    if (npciTokenResponse.npciToken.token == null) {
      return false;
    }
    return true;
  }

  setMpin = (account) => {
    Olive.requestOtp({ accountId: account.referenceId }, {}, null, otpResponse => {
      if (this.wasOtpGenerationSuccessful(otpResponse.data)) {
        this.requestNpciKeys();
      } else {
        console.log("otpResponse", otpResponse.data);
        (__ROOTSCREEN).hideLoading();
        this.showToast("Error generating otp");
      }
    }, err => {
      (__ROOTSCREEN).hideLoading();
      console.log("Error requesting otp ", err);
      this.showToast("Error generating otp");
    })
  };

  requestNpciKeys = () => {
    Olive.listKeys({}, {}, null, keyResponse => {
      if (this.wasListKeysSuccessful(keyResponse.data)) {
        this.npciKeys = keyResponse.data.npciKeys.data;
        this.getNpciToken();
      } else {
        (__ROOTSCREEN).hideLoading();
        console.log("list Key response ", keyResponse);
        this.showToast("Internal Server Error");
      }
    }, err => {
      (__ROOTSCREEN).hideLoading();
      console.log("Error fetching keys ", err);
      this.showToast("Internal Server Error");
    })
  }

  requestNpciToken = () => {
    Olive.getNpciToken({}, { type: this.type, challenge: this.challenge }, null, npciTokenResponse => {
      if (this.wasNpciTokenFetchSuccessful(npciTokenResponse.data)) {
        JBridge.setKey("npciToken", npciTokenResponse.data.npciToken.token);
        this.registerAppWithNpci();
      } else {
        (__ROOTSCREEN).hideLoading();
        console.log("npci token response ", npciTokenResponse);
        this.showToast("Internal Server Error");
      }
    }, err => {
      (__ROOTSCREEN).hideLoading();
      console.log("Error getting npci token ", err);
      this.showToast("Internal Server Error");
    })
  }

  challengeCallback = callbackMapper.map((params) => {
    this.challenge = params;
    this.requestNpciToken();
  });

  getNpciChallenge = () => {
    (__ROOTSCREEN).hideLoading();
    this.type = "initial";
    NPCICL.getChallenge("initial", JBridge.getDeviceId(), this.challengeCallback);
  };

  getNpciToken = () => {
    if (JBridge.getKey("npciToken", null) == null) {
      this.getNpciChallenge();
    } else {
      this.createVpa();
    }
  };

  getNpciCredPayload = () => {

  };

  onCredBlockReceived = callbackMapper.map((payload) => {
    console.log("response -- > ", payload);
  });

  guid = () => {
    function s4() {
      return Math.floor((1 + Math.random()) * 0x10000)
        .toString(16)
        .substring(1);
    }
    return s4() + s4() + s4() + s4() +
      s4() + s4() + s4() + s4();
  }

  //TODO MobileNumber should be stored seperately
  invokeNpci = () => {
    let credAllowedString = "{\n" + "\t\"CredAllowed\": [{\n" + "\t\t\"type\": \"OTP\",\n" + "\t\t\"subtype\": \"SMS\",\n" + "\t\t\"dType\": \"NUM\",\n" + "\t\t\"dLength\": 6\n" + "\n" + "},{ " + "\t\t\"type\": \"PIN\",\n" + "\t\t\"subtype\": \"MPIN\",\n" + "\t\t\"dType\": \"" + "NUM" + "\",\n" + "\t\t\"dLength\": 6  \n" + "\t}]\n" + "\n" + "}";
    let uniqueId = this.guid();
    let credPayload = {
      token: JBridge.getKey("npciToken", null),
      credAllowed: credAllowedString,
      txnId: uuid,
      custMobileNo: JBridge.getKey("userName", null);
      key: this.npciKeys,
      configuration: {
        payerBankName: "Axis Bank Ltd.",
        color: "#CD1C5F",
        backgroundColor: "#FFFFFF"
      }
    }
    let keyCode = "NPCI";
    let listKeyPayload = NPCICL.decodeNPCIXmlKeys(this.npciKeys);
    var payRequest = {};
    var salt = {
      txnId: credPayload.txnId,
      appId: JBridge.getPackageName(),
      deviceId: JBridge.getDeviceId(),
      mobileNumber: JBridge.getKey("userName", null)
    };
    const SALT_DELIMITER = "|";
    var trustStr =
      credPayload.txnId + SALT_DELIMITER +
      JBridge.getPackageName() + SALT_DELIMITER +
      credPayload.custMobileNo + SALT_DELIMITER +
      JBridge.getDeviceId()
    var trustCred = NPCICL.trustCred(trustStr, credPayload.token);
    var payInfoArray = [{ "name": "mobileNumber", "value": credPayload.custMobileNo }];
    var languagePref = "en_Us";
    var credentialCallback =
      // console.log("keyCode = ", keyCode, "listKeyPayload = ", listKeyPayload,
      //   "credPayload.credAllowed = ", credPayload.credAllowed);
      // console.log("configuration = ", JSON.stringify(credPayload.configuration),
      //   "SALT = ", JSON.stringify(salt),
      //   "payInfoArray = ", JSON.stringify(payInfoArray),
      //   "trustCred = ", trustCred);
      NPCICL.getCredentials(keyCode,
        listKeyPayload,
        credPayload.credAllowed,
        JSON.stringify(credPayload.configuration),
        JSON.stringify(salt),
        JSON.stringify(payInfoArray),
        trustCred,
        languagePref,
        this.onCredBlockReceived);
  };

  wasVpaCreationSuccessful = (createVpaResponse) => {
    if (!createVpaResponse || createVpaResponse.error) {
      return false;
    }
    if (!createVpaResponse.account || typeof createVpaResponse.account != "object") {
      return false;
    }
    return createVpaResponse.account.linkedToVpa;
  };

  createVpa = () => {
    Olive.linkVpaToAccount({ accountId: this.account.referenceId }, { vpa: this.vpa }, null, createVpaResponse => {
      console.log("createVpaResponse ", createVpaResponse.data);
      if (this.wasVpaCreationSuccessful(createVpaResponse.data)) {
        this.invokeNpci();
      } else {
        (__ROOTSCREEN).hideLoading();
        console.log("Error creating vpa", createVpaResponse);
        this.showToast("Error creating vpa");
      }
    }, err => {
      (__ROOTSCREEN).hideLoading();
      console.log("Error creating vpa", err);
      this.showToast("Error creating vpa");
    });
  };

  //TODO is any check required?
  onNpciRegistrationSuccess = callbackMapper.map((payload) => {
    console.log("NpciRegistrationSuccess");
    this.createVpa();
  });

  //TODO Null check
  registerAppWithNpci = () => {
    let mobileNo = JBridge.getKey("userName", null);
    let packageName = JBridge.getPackageName();
    let deviceId = JBridge.getDeviceId();
    let hash = NPCICL.populateHMAC(packageName, mobileNo, JBridge.getKey("npciToken", null), deviceId);
    NPCICL.registerApp(packageName, mobileNo, deviceId, hash, onNpciRegistrationSuccess);
  };

  fetchAccounts = () => {
    Olive.getAllAccounts({}, {}, "linkedToVpa=true&refresh=true&bankId=" + this.bank.referenceId,
      accountResponse => {
        (__ROOTSCREEN).hideLoading();
        this.handleAccountResponse(accountResponse.data);
      }, err => {
        (__ROOTSCREEN).hideLoading();
        console.log("Error fetching bank accounts ", err);
        this.showToast("Error fetching bank accounts");
      });
  };

  handleStateChange = (data) => {
    var cmd = '';
    switch (data.action) {
      case "LINK_ACCOUNT_SCREEN":
        cmd += this.set({
          id: this.idSet.buttonblur,
          alpha: "1"
        });
        cmd += this.set({
          id: this.idSet.image,
          imageUrl: "verification_success"
        });
        cmd += this.set({
          id: this.idSet.text,
          text: "6-digit MPIN exsist",
          color: "#27BC5C"
        });
        cmd += this.set({
          id: this.idSet.actionbutton,
          text: "RESET MPIN"
        });
        cmd += this.inScreen();
        break;
    }

    if (cmd) {
      Android.runInUI(
        cmd,
        null
      )
    }

  }

  handleClick = () => {
    this.props.showScreen("DEBIT_CARD_SCREEN", 1);
  }

  render() {
    this.layout = (
      <RelativeLayout
			clickable = "true"
			root="true"
			background="#FFFFFF"
			width="match_parent"
			height="match_parent">
				<LinearLayout
				padding="16,16,16,16" 
				height="wrap_content"
				width="match_parent"
				orientation = "vertical">
					<LinearLayout
					height="wrap_content"
					width="match_parent"
					orientation = "vertical">
					<TextView
					fontSize="2,2"
					margin="0,24,0,0"
					color="#C81B5D"
					text = {this.vpa+"@axis"}/>
					<LinearLayout
					margin="0,24,0,0"
					height="wrap_content"
					width="match_parent">
					<ImageView
					height="48"
					width="48"
					margin="0,0,24,0"
					imageUrl="axis_bank_logo"/>
					<LinearLayout
					height="wrap_content"
					width="match_parent"
					orientation = "vertical">
					<TextView
					fontSize="2,2.5"
					color="#000000"
					typeface="bold"
					text="XXXX123456"/>
					<TextView
					fontSize="2,2"
					alpha="0.5"
					typeface="bold"
					color="#000000"
					text="KR PURAM"/>
					</LinearLayout>
					</LinearLayout>
					<LinearLayout
					margin="0,12,0,0"
					height="wrap_content"
					width="match_parent"
					orientation = "horizontal">
					<ImageView
					id={this.idSet.image}
					layout_gravity="center"
					height="20"
					width="20"
					imageUrl="pending_icon"/>
					<TextView
					id={this.idSet.text}
					fontSize="2,2"
					margin="8,0,0,0"
					typeface="bold"
					color="#F49143"
					layout_gravity="center"
					text="6-digit Mpin required"/>
					<RelativeLayout
					height="wrap_content"
					width="match_parent">
					<LinearLayout
					cornerRadius="20"
					background="#FFEF2368"
					height="wrap_content"
					padding="0,10,0,10"
					gravity="center"
					onClick={this.handleClick}
					alignParentRight="true,-1"
					width="120">
					<TextView
						typeface="bold"
						color="#FFFFFF"
						text = "Set MPIN"
						fontSize="2,2"/>
					</LinearLayout>
					</RelativeLayout>
					</LinearLayout>
					</LinearLayout>
					<LinearLayout
					height="1"
					width="match_parent"
					margin="0,16,0,0"
					background="#CCCCCC"/>
				</LinearLayout>
				<RelativeLayout
					width="match_parent"
					height="match_parent">
				<LinearLayout
				alignParentBottom = "true,-1"
				width="match_parent"
				height="wrap_content"
				onClick={this.handleClick}>
			<LinearLayout
						id={this.idSet.submit}
						onClick={this.next}
						clickable="false"
						height="48"
						gravity="center"
						width="match_parent"
						alignParentBottom="true,-1"
						background="#CCCCCC">
					<TextView
					id={this.idSet.text}
					typeface="bold"
					fontSize="2,2.2"
					color="#FFFFFF"
					text="PROCEED"/>
				</LinearLayout>
			</LinearLayout>		
				</RelativeLayout>
			</RelativeLayout>
    )

    return this.layout.render();
  }
}

module.exports = Connector(LinkAccountScreen);
