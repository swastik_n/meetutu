var dom = require("@juspay/mystique-backend").doms.android;
var Connector = require("@juspay/mystique-backend").connector;
var View = require("@juspay/mystique-backend").baseViews.AndroidBaseView;
var LinearLayout = require("@juspay/mystique-backend").androidViews.LinearLayout;
var RelativeLayout = require("@juspay/mystique-backend").androidViews.RelativeLayout;
var FrameLayout = require("@juspay/mystique-backend").androidViews.FrameLayout;

const CURRENT_SCREEN = "ROOT_SCREEN";

class RootScreen extends View {
	constructor(props, child, state) {
		super(props, child);
		this.child = child;
		this.state = state;
		this.setIds([
			'root',
		]);
		window.handleBackPress = this.handleBackPress;
	}

	handleStateChange = (data) => {
		var result = this.child.handleStateChange(data);
		if(result){
			return {runInUI : result.runInUI}	
		}
		return {}		
	}

	handleBackPress = () => {
    this.props.showScreen("SHOW_PREVIOUS_SCREEN", {});
  }
	
	render() {
		this.layout = (
			<FrameLayout
				root="true" 
				width="match_parent" 
				height="match_parent">
				<RelativeLayout 
					width="match_parent" 
					height="match_parent">
					{this.child}
				</RelativeLayout>			
			</FrameLayout>
		)
		return this.layout.render();
	}

}

module.exports = Connector(RootScreen);