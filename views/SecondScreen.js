var dom = require("@juspay/mystique-backend").doms.android;
var Connector = require("@juspay/mystique-backend").connector;
var View = require("@juspay/mystique-backend").baseViews.AndroidBaseView;
var RelativeLayout = require("@juspay/mystique-backend").androidViews.RelativeLayout;
var ImageView = require("@juspay/mystique-backend").androidViews.ImageView;
var EditText = require("@juspay/mystique-backend").androidViews.EditText;
var TextView = require("@juspay/mystique-backend").androidViews.TextView;
var Button = require("@juspay/mystique-backend").androidViews.Button;
var ScrollView = require("@juspay/mystique-backend").androidViews.ScrollView;
var LinearLayout = require("@juspay/mystique-backend").androidViews.LinearLayout;
// const CURRENT_SCREEN = "SECOND_SCREEN";

class SecondScreen extends View {
	
	constructor(props, children, state) {
		// console.log("WELCOME TO SECOND SCREEN");
		super(props, children);
		this.state = state;
		console.log("STATE IN 2nd screen: " + this.state);
		this.setIds([
			'maps'
		]);
	}


	handleStateChange = (data) => {
		this.state = data.state;
		var cmd= '';	
		console.log("DATA STATE IN 2nd screen: " + data);
		switch(data.state.local.nextAction) {
			case "SHOW_NEXT_SCREEN" :
				this.props.showScreen("HELLO_WORLD_SCREEN",{});
				console.log("SIGN_UP_SCREEN");
				break;
			default :
        throw new Error("Next action not handled "+ data.state.nextAction);
        break;
		}	
		return {runInUI : cmd};
	}

	handleBackPress = () => {
    this.props.showScreen("SHOW_PREVIOUS_SCREEN", {});
  }

	render() {
		var width = window.__WIDTH + '';
		var currLat = 20.3187387;
		var currLong = 85.812153;
		var mapStr = "http://maps.googleapis.com/maps/api/staticmap?zoom=15&scale=1&maptype=roadmap&markers=icon:http://goo.gl/QPVYcM%7Cshadow:false%7C" + currLat + "," + currLong + "&size=600x550";
		console.log(mapStr);
		this.layout = (
			<LinearLayout 
				width="match_parent" 
				height="match_parent"
				background="#FFFFFF"
				orientation="vertical"
				root="true">

				<ImageView 
                        url={mapStr}
                        height="96dp"
                        width="96dp" />
				<ImageView

                        height="96dp"
                        width="96dp" 
					imageUrl="teacher" />
				<ImageView

                        height="96dp"
                        width="96dp" 
					imageUrl="learner" />
				<ImageView

                        height="96dp"
                        width="96dp" 
					imageUrl="learner" />
				<ImageView
				
                        height="96dp"
                        width="96dp" 
					imageUrl="teacher" />	
				<Button
					width="wrap_content"
					height="wrap_content"
					text="SIGN OUT"
					onClick={this.handleBackPress}/>
			</LinearLayout>
		)	
		return this.layout.render();
	}
}

module.exports = Connector(SecondScreen);