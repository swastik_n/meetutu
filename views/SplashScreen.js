var dom = require("@juspay/mystique-backend").doms.android;
var Connector = require("@juspay/mystique-backend").connector;
var View = require("@juspay/mystique-backend").baseViews.AndroidBaseView;
var RelativeLayout = require("@juspay/mystique-backend").androidViews.RelativeLayout;
var ImageView = require("@juspay/mystique-backend").androidViews.ImageView;
const Olive = require("../services/Olive.js");
const callbackMapper = require("@juspay/mystique-backend/").helpers.android.callbackMapper;

window.__Olive = Olive;

class SplashScreen extends View {
    constructor(props, children) {
        super(props, children);
        var cmd='';
        Android.runInUI(cmd, null);
    }


    handleStateChange = (data) => {
        var cmd = '';
        setTimeout(() => {
                        this.props.showScreen("HELLO_WORLD_SCREEN", {
                            direction:1,
                            addToStack:false
                        });
                    }, 2000);
    }

    render(state) {
        this.layout = (
            <RelativeLayout
        afterRender={this.afterRender}
        clickable="true" 
        height="match_parent" 
        orientation="vertical"
        width="match_parent"
        root="true"
        gravity="center"
        background="#FFFFFF">
        <ImageView
          width="match_parent"
          height="match_parent"
          imageUrl="splash_screen_img"/>
      </RelativeLayout>)

        return this.layout.render();
    }
}

module.exports = Connector(SplashScreen);