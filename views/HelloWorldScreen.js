var dom = require("@juspay/mystique-backend").doms.android;
var Connector = require("@juspay/mystique-backend").connector;
var View = require("@juspay/mystique-backend").baseViews.AndroidBaseView;
var RelativeLayout = require("@juspay/mystique-backend").androidViews.RelativeLayout;
var ImageView = require("@juspay/mystique-backend").androidViews.ImageView;
var EditText = require("@juspay/mystique-backend").androidViews.EditText;
var TextView = require("@juspay/mystique-backend").androidViews.TextView;
var Button = require("@juspay/mystique-backend").androidViews.Button;
var ScrollView = require("@juspay/mystique-backend").androidViews.ScrollView;
var LinearLayout = require("@juspay/mystique-backend").androidViews.LinearLayout;
// const CURRENT_SCREEN = "HELLO_WORLD_SCREEN";


class HelloWorldScreen extends View {
	
	constructor(props, children, state) {
		super(props, children);
		this.state = state;
		this.setIds([
			'email',
            'name',
            'mobileno',
            'teach',
            'learn',
            'dummy'
		]);
	}

	handleStateChange = (data) => {
		this.state = data.state;
		var cmd= '';
        // console.log("handleStateChange: " + data.state);
		switch(data.state.local.nextAction) {
			case "SHOW_NEXT_SCREEN" :
				this.props.showScreen("SHOW_SECOND_SCREEN",{});
				// console.log("SHOW_SECOND_SCREEN");
				break;
            case "SHOW_LOGIN_SCREEN" :
                this.props.showScreen("LOGIN_SCREEN", {});
			default :
        throw new Error("Next action not handled "+ data.state.nextAction);
        break;
		}	
		
		if(cmd){
			return {runInUI : cmd}
		}
        else {
            return {};
        }
	}

	proceed = () => {
		this.props.proceed(this.email, this.name, this.mobileno, this.teach, this.learn);
	}

    onLayoutClick = () => {
        // console.log("Inside on Layout Click");
        var cmd = "";
        cmd += this.set({
            id: this.idSet.dummy,
            focus: "true"
        });
        Android.runInUI(cmd, null);
    }

    name = (data) => {
        this.name = data;
    }

    email = (data) => {
        this.email = data;
    }

    mobile = (data) => {
        this.mobileno = data;
    }

    proceed = () => {
        console.log("Clicked signup" + "\n"
            + this.name + ";"
            + this.email + ";"
            + this.mobileno + ";"
            + this.teach + ";"
            + this.learn);
        this.props.proceed(this.email, this.name, this.mobileno, this.teach, this.learn);
    }

    proceedLogin = () => {
        
        this.props.proceedLogin("Going to Login");
    }

	render() {
		var width = window.__WIDTH + '';
		this.layout = (
		<RelativeLayout
            clickable="true"
            cornerRadius="1"
            padding="32, 16, 32, 16"
            width="match_parent"
            height="match_parent"
            root="true">
            

            <ScrollView
                height="match_parent"
                width="match_parent"
                scrollbars="none">

                <LinearLayout
                    height="wrap_content"
                    width="match_parent"
                    onClick={this.onLayoutClick}
                    gravity="center"
                    orientation="vertical">
                    
                    <ImageView 
                        imageUrl="app_img2"
                        height="96"
                        width="96" />

                    <EditText
                        id={this.idSet.email}
                        onChange={this.email}
                        width="match_parent"
                        height="wrap_content"
                        hint="Email"
                        margin="0,16,0,0" />

                    <EditText
                        id={this.idSet.name}
                        onChange={this.name}
                        width="match_parent"
                        height="wrap_content"
                        hint="Name"
                        margin="0,16,0,0" />

                    <EditText
                        id={this.idSet.mobileno}
                        onChange={this.mobileno}
                        width="match_parent"
                        height="wrap_content"
                        hint="Mobile No"
                        margin="0,16,0,0"/>

                    
                    <EditText
                        id={this.idSet.teach}
                        width="match_parent"
                        height="wrap_content"
                        hint="I can teach"
                        margin="0,16,0,0" />

                    <EditText
                        id={this.idSet.learn}
                        width="match_parent"
                        height="wrap_content"
                        hint="I want to learn"
                        margin="0,16,0,0" />

                    <LinearLayout
                        width="match_parent"
                        height="wrap_content"
                        orientation="horizontal">
                        <LinearLayout
                        width="wrap_content"
                        height="wrap_content"
                        orientation="vertical">
                            <Button
                                onClick={this.proceed}
                                width="wrap_content"
                                height="wrap_content"
                                text="SIGN UP"
                                color="#ffffff"
                                background="#1B5E20"
                                padding="32,0,32,0"
                                margin="0,32,0,0" />
                        
                            <TextView
                                width="wrap_content"
                                height="wrap_content"
                                text="Terms and Conditions"
                                textDecoration="underline"
                                margin="0,8,0,8" />
                        </LinearLayout>

                        <LinearLayout
                        width="match_parent"
                        height="wrap_content"
                        orientation="vertical">
                            <TextView
                                width="wrap_content"
                                height="wrap_content"
                                text="Existing user?"
                                textDecoration="underline"
                                margin="32,32,0,0" />
                            <Button
                                onClick={this.proceedLogin}
                                width="wrap_content"
                                height="wrap_content"
                                text="LOGIN"
                                color="#ffffff"
                                background="#1B5E20"
                                margin="32,8,0,0" />
                        
                        </LinearLayout>
                    </LinearLayout>
                </LinearLayout>
            </ScrollView>
        </RelativeLayout>
		)
		return this.layout.render();
	}
}

module.exports = Connector(HelloWorldScreen);