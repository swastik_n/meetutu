const axios = require("axios");
const env = "development";
const config = require("../config/config.js")[env];
const backend = config.backend;
const R = require("ramda");
const endpoints = backend.endpoints;

const Olive = axios.create({
  baseURL: backend.domain,
  timeout: backend.domain,
});

const getBindData = (state) => {
  return "data=" + encodeURIComponent(JBridge.getEncryptedBindPayload("pp1234"));
};

const bindDevice = (state, onSuccess, onFailure) => {
  return Olive.request({
      method: "POST",
      url: backend.domain + endpoints["bind"],
      headers: { 'Content-Type': 'application/json', 'X-MAuth-Token': JBridge.getKey("X-MAuth-Token", "anytoken") },
      data: state
    }).then(function(response) {
      onSuccess(response);
    })
    .catch(onFailure)
};

const tokenStatus = (state, onSuccess, onFailure) => {
  return Olive.request({
      method: "GET",
      url: backend.domain + endpoints["tokenStatus"] + state + "?embed=appUser",
      headers: { "X-MAuth-Token": JBridge.getKey("X-MAuth-Token", "anytoken") }
    }).then(function(response) {
      onSuccess(response);
    })
    .catch(onFailure)
};

const registration = (state, authToken, onSuccess, onFailure) => {
  return Olive.request({
      method: "POST",
      url: backend.domain + endpoints["registration"],
      headers: { "X-MAuth-Token": JBridge.getKey("X-MAuth-Token", "anytoken"), "X-CAuth-Token": authToken, "Content-Type": "application/json" },
      data: state,
    }).then(function(response) {
      onSuccess(response);
    })
    .catch(onFailure)
};

const changePasscode = (state, authToken, loginToken, userName, onSuccess, onFailure) => {
  return Olive.request({
      method: "POST",
      url: backend.domain + endpoints["changePasscode"] + userName,
      headers: { "X-MAuth-Token": JBridge.getKey("X-MAuth-Token", "anytoken"), "X-CAuth-Token": authToken, "Content-Type": "application/json" ,"X-Login-Token" : loginToken},
      data: state,
    }).then(function(response) {
      onSuccess(response);
    })
    .catch(onFailure)
};


const merchantCustomerAuthenticatedReq = (serviceName, method) => {
  return (reqParams, reqBody, queryParams, onSuccess, onFailure) => {
    let url = queryParams ? backend.domain + endpoints[serviceName] + "?" + queryParams : backend.domain + endpoints[serviceName];
    let reqParamKeys = Object.keys(reqParams);
    console.log("Object Params Keys",reqParamKeys);
    for(var i = 0; i < reqParamKeys.length ; i++) {
      let key = reqParamKeys[i];
      url = url.replace(":" + key, reqParams[key]);
    }
    // for (let key of reqParamKeys) {
    //   url = url.replace(":" + key, reqParams[key]);
    // }
    return Olive.request({
      url: url,
      method: method,
      headers: {
        "X-MAuth-Token": JBridge.getKey("X-MAuth-Token", null),
        "X-CAuth-Token": JBridge.getKey("registrationToken", null)
      },
      data: reqBody || {}
    }).then(onSuccess).catch(onFailure)
  };
};

const axisPayCustomerAuthenticatedReq = (serviceName, method) => {
  return (reqParams, reqBody, queryParams, onSuccess, onFailure) => {
    let url = queryParams ? backend.domain + endpoints[serviceName] + "?" + queryParams : backend.domain + endpoints[serviceName];
    let reqParamKeys = Object.keys(reqParams);
    for(var i = 0; i < reqParamKeys.length ; i++) {
      let key = reqParamKeys[i];
      url = url.replace(":" + key, reqParams[key]);
    }
    // for (let key of reqParamKeys) {
    //   url = url.replace(":" + key, reqParams[key]);
    // }
    return Olive.request({
      url: url,
      method: method,
      headers: {
        "X-MAuth-Token": JBridge.getKey("X-MAuth-Token", null),
        "X-CAuth-Token": JBridge.getKey("registrationToken", null),
        "X-Login-Token": JBridge.getKey("loginToken", null)
      },
      data: reqBody || {}
    }).then(onSuccess).catch(onFailure)
  };
};

const listKeys = merchantCustomerAuthenticatedReq("listKeys", "GET");
const getAllBanks = merchantCustomerAuthenticatedReq("getAllBanks", "GET");
const getNpciToken = merchantCustomerAuthenticatedReq("getNpciToken", "POST");
const requestOtp = merchantCustomerAuthenticatedReq("requestOtp", "POST");
const changeMpin = merchantCustomerAuthenticatedReq("changeMpin", "POST");
const getAllAccounts = merchantCustomerAuthenticatedReq("getAllAccounts", "GET");
const pay = merchantCustomerAuthenticatedReq("pay", "POST");
const transactionsList = merchantCustomerAuthenticatedReq("pay", "GET");
const transactionStatus = merchantCustomerAuthenticatedReq("transactionStatus", "POST");
const transactionRequests = merchantCustomerAuthenticatedReq("transactionRequests", "POST");
const declineTransaction = merchantCustomerAuthenticatedReq("declineTransaction", "POST");
const login = merchantCustomerAuthenticatedReq("login", "POST");
const updateUser = axisPayCustomerAuthenticatedReq("updateUser", "POST");
const listContacts = axisPayCustomerAuthenticatedReq("listContacts", "GET");
const addContact = axisPayCustomerAuthenticatedReq("listContacts", "POST");
const deleteContact = axisPayCustomerAuthenticatedReq("updateContact", "DELETE");
const updateContact = axisPayCustomerAuthenticatedReq("updateContact", "PATCH");
const verifyVpa = merchantCustomerAuthenticatedReq("verifyVpa", "GET");
const listVpa = merchantCustomerAuthenticatedReq("listVpa", "GET");
const linkVpaToAccount = merchantCustomerAuthenticatedReq("linkVpaToAccount", "POST");
const getQuery = merchantCustomerAuthenticatedReq("query", "GET");
const addQuery = merchantCustomerAuthenticatedReq("query", "POST");

module.exports = {
  bindDevice: bindDevice,
  tokenStatus: tokenStatus,
  registration: registration,
  login: login,
  changePasscode: changePasscode,
  updateUser: updateUser,
  getAllBanks: getAllBanks,
  listKeys: listKeys,
  getNpciToken: getNpciToken,
  requestOtp: requestOtp,
  changeMpin: changeMpin,
  getAllAccounts: getAllAccounts,
  pay: pay,
  transactionsList: transactionsList,
  transactionStatus: transactionStatus,
  transactionRequests: transactionRequests,
  declineTransaction: declineTransaction,
  listContacts: listContacts,
  addContact: addContact,
  deleteContact: deleteContact,
  updateContact: updateContact,
  verifyVpa: verifyVpa,
  listVpa: listVpa,
  linkVpaToAccount: linkVpaToAccount,
  getQuery : getQuery,
  addQuery : addQuery
};
