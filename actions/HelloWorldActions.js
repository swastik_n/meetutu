module.exports = (dispatcher) => {
	return {
		showScreen : (action, payload) => {
			//console.log("showScreen " ,action "Payload ",payload );
			dispatcher("SCREEN", action, payload)
		},
		// sayHello : (payload) => {
		// 	dispatcher("HELLO_WORLD_SCREEN", "STORE_DATA", {message : "Hello World!"});
		// },
		proceed : (name, email, mobileno, teach, learn) => {
			dispatcher("HELLO_WORLD_SCREEN", "STORE_RESPONSE", {message :
															email,
															name,
															mobileno,
															teach,
															learn
														});
		},

		proceedLogin : () => {
			dispatcher("HELLO_WORLD_SCREEN", "STORE_LOGIN_RESPONSE", {message : ""});
		}
	};
}