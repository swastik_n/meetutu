const callbackMapper = require("@juspay/mystique-backend/").helpers.android.callbackMapper;

module.exports = (dispatcher) => {
  return {
    showScreen: (action, payload) => {
      dispatcher("SCREEN", action, payload)
    },
    permissionCheck: () => {
      var callback = callbackMapper.map((params) => {
        console.log("Params of permission",params);
        if(params.STATUS === "SUCCESS"){
          dispatcher("SPLASH_SCREEN", "PERMISSION_CHECK", params);
        }else{
          console.log("Error In Permission Check");
          dispatcher("SPLASH_SCREEN","PERMISSION_DENIED", params);
        }
        
      });
      JBridge.permission(callback);
    },
    deviceBinding: (action, payload) => {
      dispatcher("SPLASH_SCREEN", action, payload);
    }
  };
}