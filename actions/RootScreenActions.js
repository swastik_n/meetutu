const callbackMapper = require("@juspay/mystique-backend").helpers.android.callbackMapper;

module.exports = (dispatcher) => {
	return {
		showScreen : (action, payload) => {
			dispatcher("SCREEN", action, payload)
		},
		endTransaction : (payload) => {
			var sessionData = UPI.getSessionParams(); 
			var jsonParsedsessionData = JSON.parse(sessionData);
			
			var errorCode = "1001";
			var status = "FAILURE";
			var errorDescription = "User Aborted.";

			if(payload && payload.errorCode && payload.status && payload.errorDescription) {
				errorCode = payload.errorCode;
				status = payload.status;
				errorDescription = payload.errorDescription;
			}
			
			var callback = callbackMapper.map((params) => {
				console.log("endTransaction", params);
				UPI.endTransaction(JSON.stringify({
					errorCode : errorCode,
          status : status, 
          errorDescription : errorDescription,
          uniquetransactionID : jsonParsedsessionData["unqTxnId"]
        }));
			});
			NPCICL.unbindNPCICL(callback);	
		}
	};
}
