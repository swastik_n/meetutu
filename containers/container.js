//Logger
//const logger = require("../utils/logger");

//Reducer
const ScreenReducer = require("../state_machines/Screens");
const HelloWorldScreenReducer = require("../state_machines/HelloWorldState");
const SecondScreenReducer = require("../state_machines/SecondScreenState");
const SplashScreenReducer = require("../state_machines/SplashScreenState");
const LoginScreenReducer = require("../state_machines/LoginScreenState");


const reducer = require("@juspay/mystique-backend").stateManagers.reducer({
	"SCREEN" : ScreenReducer,
	"SPLASH_SCREEN" : SplashScreenReducer,
	"HELLO_WORLD_SCREEN" : HelloWorldScreenReducer,
	"SECOND_SCREEN" : SecondScreenReducer,
	"LOGIN_SCREEN" : SecondScreenReducer
});

const uiHandler = require("@juspay/mystique-backend").uiHandlers.android;
var dispatcher;

// Screens
const RootScreen = require("../views/RootScreen");
const SplashScreen = require("../views/SplashScreen");
const HelloWorldScreen = require("../views/HelloWorldScreen");
const SecondScreen = require("../views/SecondScreen");
const LoginScreen = require("../views/LoginScreen");

// ScreenActions
const RootScreenActions = require("../actions/RootScreenActions");
const SplashScreenActions = require("../actions/SplashScreenActions");
const HelloWorldActions = require("../actions/HelloWorldActions");
const SecondScreenActions = require("../actions/SecondScreenActions");
const LoginScreenActions = require("../actions/LoginScreenActions");


var determineScreen = (state, dispatcher) => {
	var screen;
	switch (state.global.currScreen) {
		case "SPLASH_SCREEN" : 
		 		screen = new (SplashScreen(dispatcher, SplashScreenActions))(null, null, state);
		 		break;
		case "HELLO_WORLD_SCREEN" : 
		 		screen = new (HelloWorldScreen(dispatcher, HelloWorldActions))(null, null, state);
		 		break;
	 	case "SECOND_SCREEN" : 
	 		console.log("SECOND_SCREEN ");
	 		screen = new (SecondScreen(dispatcher, SecondScreenActions))(null, null, state);
	 		break;
	 	case "LOGIN_SCREEN" : 
	 		console.log("LOGIN_SCREEN ");
	 		screen = new (LoginScreen(dispatcher, LoginScreenActions))(null, null, state);
	 		break; 
	 	default : 
	 		throw new Error("Current screen not handled "+ state.global.currScreen);
	 		break;
	}

	return screen;
}

var res;
var currView;
var CURRENT_SCREEN = null;

var Containers = {
	handleStateChange : (data) => {
		var state = data.state.global;
		console.log("STATE : " , data.state);
		if(state.currScreen) {
			console.log("SCREEN : ",state.currScreen);
			currView = (new (RootScreen(dispatcher, RootScreenActions))({}, determineScreen(data.state, dispatcher), data));
			return { render : currView.render()};
		} else {
			return currView.handleStateChange(data) || {};
		}
	}
}

dispatcher = require("@juspay/mystique-backend").stateManagers.dispatcher(Containers, uiHandler, reducer);

module.exports = {
	init : () => {
		dispatcher("SCREEN", "INIT_UI", {});
	}
}